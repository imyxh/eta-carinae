" this file should reside in
" ~/.config/nvim/colors/
" or
" ~/.vim/colors/

" local syntax file - set colors on a per-machine basis:
" Vim color file
" This is a modified copy of the Zellner color scheme
" Maintainer: Ian Huang <imyxh>

" better hope your terminal doesn't suck!
set termguicolors

set background=dark
"hi clear (breaks airline themes, etc.)
if exists("syntax_on")
    syntax reset
endif
let g:colors_name = "eta-carinae"

hi Normal ctermfg=White guifg=#D9D9D9 guibg=NONE
hi Comment cterm=italic ctermfg=DarkGray gui=italic guifg=#999999

" main two colors:
hi Constant term=underline ctermfg=Magenta guifg=#F86D8B
hi Function ctermfg=Blue guifg=#BD9AEF

hi Error term=reverse ctermfg=15 ctermbg=9 guibg=Red guifg=White
hi Search ctermfg=Black ctermbg=Gray gui=NONE guifg=Black guibg=LightGray
hi Statement term=bold cterm=bold ctermfg=White gui=Bold guifg=White
hi StatusLine term=bold,reverse cterm=NONE ctermfg=Yellow ctermbg=DarkGray gui=NONE guifg=Gray guibg=#222222
hi Tag term=bold ctermfg=DarkGreen guifg=DarkGreen
hi Todo term=standout ctermbg=Yellow ctermfg=Black guifg=Black guibg=#FFBF66
hi Underlined cterm=underline ctermfg=Gray gui=underline guifg=#D9D9D9
hi Visual term=reverse ctermfg=Black ctermbg=Yellow gui=NONE guibg=#FFBF66 guifg=Black
hi WildMenu guibg=Gray guifg=Black

" GAS (use:
" https://github.com/calculuswhiz/vim-GAS-x86_64-highlighter/blob/master/syntax/GAS.vim)
hi! link gasInstr Statement
hi! link gasLabel Function
hi! link gasLiteral Constant

" julia
" fix \euler being invisible
hi! link juliaPossibleEuler Constant

hi! link ErrorMsg Visual
hi! link helpHyperTextJump Function
hi! link Identifier Normal
hi! link MoreMsg Comment
hi! link PreProc Constant
hi! link Question Comment
hi! link Special Normal
hi! link Type Normal
hi! link WarningMsg ErrorMsg
hi! link Title Statement
hi! link String Constant
hi! link Character Constant
hi! link Number Constant
hi! link Boolean Constant
hi! link Float Number
hi! link Conditional Statement
hi! link Repeat Statement
hi! link Label Statement
hi! link Operator Statement
hi! link Keyword Statement
hi! link Exception Statement
hi! link Include PreProc
hi! link Define PreProc
hi! link Macro PreProc
hi! link PreCondit PreProc
hi! link StorageClass Type
hi! link Structure Type
hi! link Typedef Type
hi! link SpecialChar Special
hi! link Delimiter Special
hi! link SpecialComment Special
hi! link Debug Special
hi! link xmlEndTag xmlTagName

" bars
hi TabLine cterm=NONE ctermfg=White ctermbg=Black gui=NONE guibg=Black
hi TabLineFill cterm=NONE ctermfg=Black ctermbg=Black gui=NONE guibg=Black
hi TabLineSel ctermbg=DarkGray guibg=DarkGray
hi ColorColumn ctermbg=Black guibg=#303030
hi VertSplit cterm=NONE gui=NONE ctermfg=White guifg=White

" menu for YCM, etc.
hi Pmenu ctermfg=Gray ctermbg=Black guibg=#202020
hi PmenuSel ctermfg=Black ctermbg=Gray guifg=Black guibg=#D9D9D9
hi PmenuSbar ctermbg=Gray guibg=Gray
hi PmenuThumb ctermbg=White guibg=White

" diff mode
hi DiffAdd cterm=NONE ctermfg=NONE ctermbg=23 guifg=NONE guibg=#005F5F
hi DiffChange cterm=NONE ctermfg=NONE ctermbg=236 guifg=NONE guibg=#303030
hi DiffText cterm=bold ctermfg=NONE ctermbg=236 gui=bold guifg=NONE guibg=#303030
hi DiffDelete cterm=NONE ctermfg=NONE ctermbg=53 guifg=NONE guibg=#5f005F

" folds
hi Folded ctermfg=249 ctermbg=238 guibg=#303030 guifg=#D0D0D0

" spellcheck (turn off for individual files with `:setlocal nospell`
set spellcapcheck= " turn off capitalization spellcheck
hi clear SpellBad
hi SpellBad cterm=undercurl gui=undercurl

" whitespace display options
set listchars=eol:¬,tab:>·,trail:·,extends:>,precedes:<, ",space:␣
set list
hi NonText ctermfg=DarkGray guifg=#444444
hi Whitespace ctermfg=DarkGray guifg=#444444

" line numbers
hi LineNr ctermfg=Gray guifg=Gray
hi CursorLineNr gui=bold ctermfg=White guifg=Gray

