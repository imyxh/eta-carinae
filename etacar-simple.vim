" this file should reside in
" ~/.config/nvim/colors/
" or
" ~/.vim/colors/

" local syntax file - set colors on a per-machine basis:
" vim: tw=0 ts=4 sw=4
" Vim color file
" This is a modified copy of the Zellner color scheme
" Maintainer: Ian Huang <imyxh>

" this is a simpler version, meant to work across more terminals and fonts

set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name = "eta-carinae"

" syntax
hi Normal guifg=#d9d9d9 guibg=NONE
hi Comment cterm=NONE ctermfg=LightGray gui=NONE guifg=#999999
hi Constant term=underline ctermfg=Magenta guifg=#f86d8b
hi Function ctermfg=Blue guifg=#bd9aef
hi Statement term=bold cterm=bold ctermfg=White gui=Bold guifg=White
hi Visual term=reverse ctermfg=0 ctermbg=9 gui=NONE guibg=#ffbf66 guifg=Black
hi Search term=reverse ctermfg=Black ctermbg=LightGray gui=NONE guifg=Black guibg=LightGray
hi Tag term=bold ctermfg=DarkGreen guifg=DarkGreen
hi Error term=reverse ctermfg=15 ctermbg=9 guibg=Red guifg=White
hi Todo term=standout ctermbg=Yellow ctermfg=Black guifg=Black guibg=#ffbf66
hi StatusLine term=bold,reverse cterm=NONE ctermfg=Yellow ctermbg=DarkGray gui=NONE guifg=LightGray guibg=#222222
hi WildMenu guibg=LightGray guifg=Black

hi! link ErrorMsg Visual
hi! link helpHyperTextJump Function
hi! link Identifier Normal
hi! link MoreMsg Comment
hi! link PreProc Constant
hi! link Question Comment
hi! link Special Normal
hi! link Type Normal
hi! link WarningMsg ErrorMsg
hi! link Title Statement
hi link String Constant
hi link Character Constant
hi link Number Constant
hi link Boolean Constant
hi link Float Number
hi link Conditional Statement
hi link Repeat Statement
hi link Label Statement
hi link Operator Statement
hi link Keyword Statement
hi link Exception Statement
hi link Include PreProc
hi link Define PreProc
hi link Macro PreProc
hi link PreCondit PreProc
hi link StorageClass Type
hi link Structure Type
hi link Typedef Type
hi link SpecialChar Special
hi link Delimiter Special
hi link SpecialComment Special
hi link Debug Special
hi link xmlEndTag xmlTagName

" bars and airline
hi TabLine cterm=NONE ctermfg=White ctermbg=Black gui=NONE guibg=Black
hi TabLineFill cterm=NONE ctermfg=Black ctermbg=Black gui=NONE guibg=Black
hi TabLineSel ctermbg=DarkGray guibg=DarkGray
hi ColorColumn ctermbg=8 guibg=#3a3a3a
hi VertSplit cterm=NONE gui=NONE ctermfg=White guifg=White
let g:airline_powerline_fonts=1
let g:airline_theme='base16_grayscale'
silent! AirlineRefresh

" menu for YCM, etc.
hi Pmenu ctermfg=0 ctermbg=13 guibg=#222222
hi PmenuSel ctermfg=242 ctermbg=0 guibg=Black
hi PmenuSbar ctermbg=248 guibg=Grey
hi PmenuThumb ctermbg=15 guibg=White

" diff mode
hi DiffAdd cterm=NONE ctermfg=NONE ctermbg=23 guifg=NONE guibg=#005f5f
hi DiffChange cterm=NONE ctermfg=NONE ctermbg=236 guifg=NONE guibg=#303030
hi DiffText cterm=bold ctermfg=NONE ctermbg=236 gui=bold guifg=NONE guibg=#303030
hi DiffDelete cterm=NONE ctermfg=NONE ctermbg=53 guifg=NONE guibg=#5f005f

" folds
hi Folded ctermfg=249 ctermbg=238 guibg=#404040 guifg=#f9f9f9

" TODO: theme vertical splits

" spellcheck (turn off for individual files with `:setlocal nospell`
set spell
set spellcapcheck= " turn off capitalization spellcheck
hi clear SpellBad
hi SpellBad cterm=undercurl gui=undercurl

" whitespace display options
set listchars=eol:¬,tab:>·,trail:·,extends:>,precedes:<, ",space:␣
set list
hi NonText ctermfg=DarkGray guifg=#444444
hi Whitespace ctermfg=DarkGray guifg=#444444

" line numbers
hi LineNr ctermfg=gray guifg=Gray
hi CursorLineNr gui=bold ctermfg=white guifg=Gray

