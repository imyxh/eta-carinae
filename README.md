Eta Carinae
===========

This is a revival of my
[LowColor](https://github.com/imyxh/lowcolor-syntax-dark) syntax project, which
aimed to reduce the amount of colors used and make use of more font styles and
weights. Eta Carinae is primarily being developed for Vim/Neovim and
GTKSourceview (which is used in GNOME Builder, gedit, etc.). Unfortunately,
there's less flexibility in assigning font weights in Vim and GTKSourceView, so
this project might not exactly live up to be the same as my Atom theme.

For Vim/Neovim, I recommend a terminal compatible with ISO-8613-3 for the use of
`termguicolors`.

Support:
  - Vim (eta-carinae.vim, etacar-c.vim)
  - rizin/radare2 (eta-carinae.rz)
  - GTKSourceView (eta-carinae.xml)
  - Irssi (eta-carinae.theme)
  - WeeChat (TODO).

To install, either copy the relevant files to their intended directories
yourself, or take a look at [install.sh](install.sh). The installation script
isn't complete, but it should give you a good idea of where things go.


Notes
-----

For 256 colors, the primary colors are 141 (purple) and 204 (pink). For
grayscale bars, I try to stick to 235 (darkgray), 238 (gray). Darkened light
text is 246. This is mostly just to document how I'm working with the WeeChat
theme.

