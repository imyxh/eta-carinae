" this file should reside in
" ~/.config/nvim/colors/
" or
" ~/.vim/colors/

" local syntax file - set colors on a per-machine basis:
" vim: tw=0 ts=4 sw=4
" Vim color file
" This is a modified copy of the Zellner color scheme
" Maintainer: Ian Huang <imyxh>

" better hope your terminal doesn't suck!
set termguicolors

set background=dark
let g:colors_name = "etacar-rosemary"

colorscheme eta-carinae

" main two colors:
hi Constant term=underline ctermfg=Green guifg=#7BAC81
hi Function ctermfg=Magenta guifg=#DBC4FC

