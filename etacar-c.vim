" this file should reside in
" ~/.config/nvim/after/syntax/c/
" or
" ~/.vim/after/syntax/c/

" TODO: consider using a project like jeaye/color_coded for clang-based
" highlighting

" highlight all function names
syn match cCustomFunc /\w\+\s*(/me=e-1,he=e-1
hi def link cCustomFunc Function

