#!/bin/sh
set -e

vimdir="${HOME}/.config/nvim"
r2dir="${HOME}/.local/share/radare2"

cat <<- INTRO

This script will attempt to install all the themes. Please ensure that the
necessary directories exist, or the script will exit.'

Writing symlinks to ...

INTRO

echo "${vimdir}/colors"
mkdir -p "${vimdir}/colors"
ln -s "${PWD}/eta-carinae.vim" ~/.config/nvim/colors/eta-carinae.vim
echo "${vimdir}/after/syntax"
mkdir -p "${vimdir}/after/syntax/c" "${vimdir}/after/syntax/sh"
ln -s "${PWD}/etacar-c.vim" ~/.config/nvim/after/syntax/c/etacar-c.vim
ln -s "${PWD}/etacar-sh.vim" ~/.config/nvim/after/syntax/sh/etacar-sh.vim

echo "${r2dir}/cons"
ln -s "${PWD}/eta-carinae.r2" ~/.local/share/radare2/cons/eta-carinae

echo 'TODO: gtksourceview'

echo
echo "Eta Carinae is now installed."
echo

