<?xml version="1.0" encoding="UTF-8"?>
<!-- vim: set shiftwidth=2: -->

<style-scheme id="eta-carinae" name="Eta Carinae" version="1.0">

  <author>Ian Huang</author>
  <description>Dark color scheme for GTKSourceView in the Eta Carinae syntax theme family.</description>

  <color name="purple"  value="#bd9aef"/>
  <color name="pink"    value="#f86d8b"/>
  <color name="select"  value="#ffbf66"/>
  <color name="white"   value="#ffffff"/>
  <color name="llgray"  value="#d9d9d9"/>
  <color name="lgray"   value="#999999"/>
  <color name="gray"    value="#7f7f7f"/>
  <color name="dgray"   value="#444444"/>
  <color name="ddgray"  value="#111111"/>
  <color name="black"   value="#000000"/>

  <!-- Global Settings -->
  <style name="text"                        foreground="llgray" background="ddgray"/>
  <style name="selection"                   foreground="black" background="select"/>
  <style name="cursor"                      foreground="white"/>
  <style name="current-line"                background="black"/>
  <style name="current-line-number"         background="black"/>
  <style name="line-numbers"                foreground="gray" background="black"/>
  <style name="draw-spaces"                 foreground="dgray"/>
  <style name="background-pattern"          background="ddgray"/>
  <style name="map-overlay"                 background="#rgba(120,120,120,0.25)"/>

  <!-- Diagnostics Underlining -->
  <style name="diagnostician::deprecated"   underline="error"/>
  <style name="diagnostician::error"        underline="error"/>
  <style name="diagnostician::note"         underline="error"/>
  <style name="diagnostician::warning"      underline="error"/>

  <!-- Snippets TODO
  <style name="snippet::tab-stop"           background="orange3" foreground="aluminium6"/>
  <style name="snippet::area"               background="#rgba(86,114,151,.5)"/>
  -->

  <!-- Debugger TODO
  <style name="debugger::current-breakpoint" foreground="#2e3436" background="#fcaf3e"/>
  <style name="debugger::breakpoint"         foreground="#ffffff" background="#204a87"/>
  -->

  <!-- Hover links -->
  <style name="action::hover-definition"    underline="true"/>

  <!-- Bracket Matching TODO (also todo on vim)
  <style name="bracket-match"               foreground="chocolate2" bold="true"/>
  <style name="bracket-mismatch"            foreground="aluminium1" background="scarletred2" bold="true"/>
  -->

  <!-- Right Margin -->
  <style name="right-margin"                foreground="black" background="black"/>

  <!-- Search Matching -->
  <style name="search-match"                foreground="aluminium1" background="chameleon3"/>
  <style name="quick-highlight-match"       background="#rgba(78,154,6,.25)"/>
  -->

  <!-- Search Shadow -->
  <style name="search-shadow"               background="#rgba(0,0,0,0.4)"/>

  <!-- Spellchecker Matching -->
  <style name="misspelled-match"            foreground="#000000" background="#b3d4fc"/>
  -->

  <!-- Comments -->
  <style name="def:comment"                 foreground="lgray"/>
  <style name="def:shebang"                 foreground="lgray" bold="true"/>
  <style name="def:doc-comment-element"     italic="true"/>

  <!-- Constants -->
  <style name="def:constant"                foreground="pink"/>
  <style name="def:string"                  foreground="pink"/>
  <style name="def:special-char"            foreground="purple"/>
  <style name="def:special-constant"        foreground="pink"/>
  <style name="def:floating-point"          foreground="pink"/>
  <style name="def:function"                foreground="purple"/>

  <!-- Identifiers -->
  <style name="def:identifier"              foreground="llgray"/>

  <!-- Statements -->
  <style name="def:statement"               foreground="white" bold="true"/>

  <!-- Types -->
  <style name="def:type"                    foreground="chameleon1" bold="true"/>

  <!-- Others -->
  <style name="def:preprocessor"            foreground="#dd4a68"/>
  <style name="def:error"                   foreground="aluminium1" background="scarletred2" bold="true"/>
  <style name="def:warning"                 foreground="aluminium1" background="plum1"/>
  <style name="def:note"                    background="butter1" foreground="aluminium4" bold="true"/>
  <style name="def:underlined"              italic="true" underline="true"/>

  <!-- Language specific -->
  <style name="c:comment"                   foreground="#8b9eab"/>
  <style name="c:preprocessor"              foreground="#8194a6" bold="false"/>
  <style name="c:boolean"                   foreground="#0077aa"/>
  <style name="c:keyword"                   foreground="#0077aa" bold="true"/>
  <style name="c:string"                    foreground="#669900"/>
  <style name="c:included-file"             foreground="orange3"/>
  <style name="c:storage-class"             foreground="orange3" bold="true"/>
  <style name="c:type"                      foreground="#669900" bold="true"/>
  <style name="c:macro-name"                foreground="#677685" bold="false"/>
  <style name="c:enum-name"                 foreground="#dd4a68" bold="false"/>

  <style name="diff:added-line"             foreground="chameleon2"/>
  <style name="diff:removed-line"           foreground="plum1"/>
  <style name="diff:changed-line"           foreground="blue1"/>
  <style name="diff:diff-file"              foreground="chameleon1" bold="true"/>
  <style name="diff:location"               foreground="chameleon1"/>
  <style name="diff:special-case"           foreground="white" bold="true"/>

  <style name="gutter:added-line"           foreground="chameleon2"/>
  <style name="gutter:changed-line"         foreground="butter3"/>
  <style name="gutter:removed-line"         foreground="scarletred3"/>

  <style name="js:object"                   foreground="chameleon3" bold="true"/>
  <style name="js:constructors"             foreground="pink1"/>
  <style name="js:keyword"                  foreground="#2b85aa"/>
  <style name="js:string"                   foreground="#669900"/>
  <style name="js:function"                 foreground="pink1"/>

  <style name="latex:command"               foreground="chameleon1" bold="true"/>
  <style name="latex:include"               use-style="def:preprocessor"/>

  <style name="xml:comment"                 foreground="#8b9eab"/>
  <style name="xml:attribute-name"          foreground="#orange3" bold="false"/>
  <style name="xml:attribute-value"         foreground="#669900"/>
  <style name="xml:tag-match"               background="#rgba(114,159,207,.20)"/>

  <!-- Symbol-tree xml-pack coloring -->
  <style name="symboltree::label"           foreground="#000000" background="#D5E7FC"/>
  <style name="symboltree::id"              foreground="#000000" background="#D9E7BD"/>
  <style name="symboltree::style-class"     foreground="#000000" background="#DFCD9B"/>
  <style name="symboltree::type"            foreground="#000000" background="#F4DAC3"/>
  <style name="symboltree::parent"          foreground="#000000" background="#DEBECF"/>
  <style name="symboltree::class"           foreground="#000000" background="#FFEF98"/>
  <style name="symboltree::attribute"       foreground="#000000" background="#F0E68C"/>

</style-scheme>

